package main

import "fmt"

func main() {
	s := []bool{true, false, true, true, false, true}
	fmt.Println(countSheep(s))
}

func countSheep(sheeps []bool) (count int) {
	for _, sheep := range sheeps {
		if sheep {
			count++
		}
	}
	return count
}
