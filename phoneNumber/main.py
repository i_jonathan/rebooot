def create_phone_number(n):
    str_list = list(map(str, n))
    return f"({''.join(str_list[0:3])}) {''.join(str_list[3:6])}-{''.join(str_list[6:10])}"


print(create_phone_number([1,2,3,4,5,6,7,8,9,0]))
