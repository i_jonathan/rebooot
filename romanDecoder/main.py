def solution(roman):
    numeral = {
            'M': 1000,
            'D': 500,
            'C': 100,
            'L': 50,
            'X': 10,
            'V': 5,
            'I': 1,
            }

    i = 0
    result = 0
    while i < len(roman):
        first = numeral[roman[i]]
        second = 0

        if i + 1 < len(roman):
            second = numeral[roman[i+1]]

        if first < second:
            result += (second - first)
            i += 2
        else:
            result += first
            i += 1

    return result

print(solution("IV"))
